// ==UserScript==
// @name     Arrowkeys Jump Opencast
// @description Use arrowkeys to jump back/forward. CC0
// @namespace   de.uni-jena.fmi.tools.arrowkeyjump-opencast
// @include     https://oc-p.*/paella/ui/watch.html?*
// @version  0.1
// @grant    none
// ==/UserScript==

console.log(JSON.stringify(GM.info));



function add_arrowkeylistener() {
  document.querySelector("#body").addEventListener("keydown", event => {
    if (event.key == "ArrowLeft") {
     document.querySelector("#buttonPlugin2").click();
    } else if (event.key == "ArrowRight") {
     document.querySelector("#buttonPlugin3").click();
    }
  })
}




setTimeout(add_arrowkeylistener, 1000);