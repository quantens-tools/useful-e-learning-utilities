// ==UserScript==
// @name     Coursename in Navigation for Moodle
// @description Adds the title field to the innerText in the Navigation pane. CC0
// @namespace   de.uni-jena.fmi.tools.coursenamenavimoodle
// @include     https://moodle.*/*
// @require https://code.jquery.com/jquery-3.5.0.slim.min.js
// @version  1
// @grant    none
// ==/UserScript==

console.log(JSON.stringify(GM.info));

elems = $("p.tree_item.branch > a[title]");
for (var elem of elems) {
  elem.innerText += " " + elem.title;
}

