// ==UserScript==
// @name     Moodle login speed up
// @description clicks on the login button to get redirected to SSO, saves 1 click. CC0
// @namespace   de.uni-jena.fmi.tools.moodleloginspeedup
// @include     https://moodle.*/login/index.php
// @require https://code.jquery.com/jquery-3.5.0.slim.min.js
// @version  1.2
// @grant    none
// ==/UserScript==

console.log(JSON.stringify(GM.info));

setTimeout(function () {
  console.log("try");
  if (document.querySelectorAll(".btn-primary").length == 1) {
    console.log("yes");
		document.querySelector(".btn-primary").click();
  }
}, 100);

