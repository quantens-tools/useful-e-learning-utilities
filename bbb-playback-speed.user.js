// ==UserScript==
// @name     PlaybackSpeed BBB
// @description Adds a slider for the playbackspeed in BBB Playbacks. CC0
// @namespace   de.uni-jena.fmi.tools.playbackspeedbbb
// @include     https://bbb.*/playback/presentation/2.0/playback.html?*
// @require https://code.jquery.com/jquery-3.5.0.slim.min.js
// @version  1.1
// @grant    none
// ==/UserScript==

console.log(JSON.stringify(GM.info));



function add_playbackspeedslider() {
  $('.acorn-timer').after('<span id="playbackspeedlabel" style="color: white;">1.0</span>');
  $('.acorn-timer').after('<div id="playbackspeeddiv" class="acorn-seek-slider ui-slider ui-widget ui-widget-content ui-corner-all ui-slider-horizontal" style="width:10%;background: inherit;"></div>');
  $('#playbackspeeddiv').append('<input type="range" min="0.5" max="3" value="1" step="0.1" class="slider" id="playbackspeedrange">');
  //$('#playbackspeeddiv').append('<label for="playbackspeedrange" id="playbackspeedlabel">1.0</label>');
  $('#playbackspeedrange' ).change(function (e, u) {
    document.getElementById("playbackspeedlabel").innerText = document.getElementById("playbackspeedrange").value;
    document.querySelector('.webcam').playbackRate = parseFloat(document.getElementById("playbackspeedrange").value);
    //console.log(document.getElementById("playbackspeedrange").value);
  });
}



setTimeout(add_playbackspeedslider, 1000);
