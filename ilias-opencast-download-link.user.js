// ==UserScript==
// @name     ILIAS-Opencast-Downloadlink
// @description Add a Download-Link to ILIAS Opencast Lectures CC0
// @namespace   edu.kit.iliasopencastlink
// @include     https://ilias.studium.kit.edu/*streamVideo*
// @require https://code.jquery.com/jquery-3.5.0.slim.min.js
// @version  1.1
// @grant    none
// ==/UserScript==

console.log(JSON.stringify(GM.info));

function add_downloadlink() {
    //stolen from https://github.com/EUB-LE/kit-video-downloader/blob/2c54794b9e0ea31e2bab060a149d3002bd83113a/content-script.js
    for (videoelem of document.getElementsByTagName("video")) {
        let videolink = videoelem.getElementsByTagName("source")[0].getAttribute("src");
        $('#playerContainer_videoContainer').prepend('<a href="' + videolink + '">DOWNLOAD VIDEO</a>');
    }
}



setTimeout(add_downloadlink, 1000);
