# Moodle Magic und mehr

E-Learning Tools wie Moodle und BigBlueButton sind schon ganz nett, aber es geht noch besser. Eine Sammlung an Tipps und Tools

Veröffentlicht unter CC0, genauso wie Wissen frei zugänglich sein sollte, sollten es auch die Werkzeuge dazu sein.

## User Scripts

Die User-Scripts sind in erster Linie für [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) geschrieben, MergeRequest für weitere Kompatibilität sind sehr gerne gesehen

### Moodle

#### Coursename in Navigation

**Funktioniert aktuell nicht:** An der Instanz, die ich nutze, wurde das Navigationspanel umgebaut, der Name ist nicht mehr im `title` Attribut zu finden. Damit funktioniert dieses Userscript nicht mehr und ein einfacher Fix ist nicht in Sicht. MergeRequests sind aber gerne gesehen :)

Im Navigationspanel werden nur die schlecht merkbare Kursnummern angezeigt, der Name ist im `title` Attribut versteckt. Dieses Userscript hängt das einfach mit an die Kursnummer an und man weiß, direkt wo man draufklicken muss.

### Login Speed Up

Klickt auf dem Button um zum SSO zu kommen. Spart bis zu zwei Klicks beim Einloggen.

### BigBlueButton

#### BBB Playback Speed

Man möchte keine Tastaturabdrücke auf der Stirn haben, weil man schon wieder beim Schauen der Vorlesungsaufzeichnung eingeschlafen ist oder die Sprache doch etwas langsamer haben?

bbb-playback-speed.user.js fügt in den BBB Playback Viewer einen Slider ein, um die Geschwindigkeit anzupassen, von 0.5x bis 3x.

### Opencast

#### Arrowkeys Jump Opencast

Der Opencast paella Player hat so schöne 10sec zurück und 30sec vor Knöpfe. Damit man die aber nicht immer mit der Maus treffen muss, mappt dieses Userscript die Pfeiltasten auf diese Knöpfe.

#### ILIAS Opencast Download Link

Zeigt ein Download-Link im Opencast-Player vom ILIAS vom KIT an.
